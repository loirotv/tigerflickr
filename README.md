# README #

InspireMe
Technical test for Tigerspike.

### What is this repository for? ###

This project has been created for a technical test of Tigerspike. 
Native Android app which is connected with Flickr's API in order to get pictures.
The aim of this app is to inspire the user about any desired word.  

### How do I get set up? ###

Clone the project and open it in Android Studio

### Who do I talk to? ###

You can send me a message to ogarcia@tueis.com