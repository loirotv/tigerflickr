package com.tigerspike.interview.oriolgarcia.tigerflickr.models;

import java.util.ArrayList;

/**
 * Created by OriolGarcia on 18/7/17.
 */
public class PictureModel {

    private String title;
    private String published;
    private String author;
    private String imageUrl;
    private ArrayList<String> tags;

    public PictureModel(String title, String published, String author, String imageUrl, ArrayList<String> tags) {
        this.title = title;
        this.published = published;
        this.author = author;
        this.imageUrl = imageUrl;
        this.tags = tags;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }
}
