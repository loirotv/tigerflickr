package com.tigerspike.interview.oriolgarcia.tigerflickr.controllers.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tigerspike.interview.oriolgarcia.tigerflickr.MainActivity;
import com.tigerspike.interview.oriolgarcia.tigerflickr.R;
import com.tigerspike.interview.oriolgarcia.tigerflickr.models.PictureModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;


/**
 * Created by OriolGarcia on 18/7/17.
 */

public class PictureAdapter extends RecyclerView.Adapter {

    private MainActivity mActivity;
    private ArrayList data = new ArrayList();
    private Context mContext;

    public PictureAdapter(MainActivity mActivity, ArrayList data, Context mContext) {
        this.mActivity = mActivity;
        this.data = data;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_picture, parent, false);
        return new PictureHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder.getClass() == PictureHolder.class) {
            final PictureModel picture = (PictureModel) data.get(position);
            final PictureHolder ph = (PictureHolder) holder;

            // Title
            if (picture.getTitle().length() > 0) {
                ph.title.setVisibility(View.VISIBLE);
                ph.title.setText(picture.getTitle());
            }

            // Get only the author username (delete email)
            String authorName = picture.getAuthor().split("[\\(\\)]")[1];
            authorName = authorName.length() > 3 ? authorName.substring(1, authorName.length() - 1) : authorName;
            ph.authorAndDate.setText(authorName + " - " + parseDateToddMMyyyy(picture.getPublished()));

            // Print tags if they exist
            if (picture.getTags().size() > 1) {
                ph.tags.setVisibility(View.VISIBLE);
                String tagsToPrint = mActivity.getResources().getString(R.string.tags) + " " + Arrays.toString(picture.getTags().toArray()).replace("[", "").replace("]", "");
                ph.tags.setText(tagsToPrint);
            }

            // Load image using Picasso library
            ph.loader.setVisibility(View.VISIBLE);
            ph.pictureImageView.setVisibility(View.GONE);
            final ProgressBar progressView = ph.loader;
            Picasso.with(mActivity)
                    .load(picture.getImageUrl())
                    .into(ph.pictureImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressView.setVisibility(View.GONE);
                            ph.pictureImageView.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError() {
                            // TODO Auto-generated method stub

                        }
                    });

            // Share image button
            ph.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, mActivity.getResources().getString(R.string.share) + " " + picture.getImageUrl());
                    mActivity.startActivity(Intent.createChooser(intent, "Share Image from TigerFlickr"));
                }
            });
        }
    }

    public static class PictureHolder extends RecyclerView.ViewHolder {
        LinearLayout pictureLayout;
        ImageView pictureImageView;
        ProgressBar loader;
        TextView title, authorAndDate, tags;
        ImageView share;

        public PictureHolder(View itemView) {
            super(itemView);
            pictureLayout = (LinearLayout) itemView.findViewById(R.id.item);
            pictureImageView = (ImageView) itemView.findViewById(R.id.picture_image_view);
            loader = (ProgressBar) itemView.findViewById(R.id.loader);
            title = (TextView) itemView.findViewById(R.id.title);
            authorAndDate = (TextView) itemView.findViewById(R.id.author_and_date);
            tags = (TextView) itemView.findViewById(R.id.tags);
            share = (ImageView) itemView.findViewById(R.id.share);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    /**
     * Parse date string to a more legible format
     *
     * @param time
     * @return date in legible format
     */
    private String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss";
        String outputPattern = "dd/MM/yyyy HH:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        String str = "";

        try {
            Date date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return str + "h";
    }
}

