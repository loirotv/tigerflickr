package com.tigerspike.interview.oriolgarcia.tigerflickr.controllers.fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.tigerspike.interview.oriolgarcia.tigerflickr.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by OriolGarcia on 18/7/17.
 */
public class InspirationFragment extends BaseFragment {

    private static final int GALLERY_FRAGMENT = 0;
    private EditText tagsToSearch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity.setFragment(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inspire_fragment, container, false);

        tagsToSearch = (EditText) view.findViewById(R.id.tags_to_search);

        Button giveInspiration = (Button) view.findViewById(R.id.give_inspiration);
        giveInspiration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.setTagsToSearch(new ArrayList<>(Arrays.asList(tagsToSearch.getText().toString().split(" "))));
                mActivity.setFragment(GALLERY_FRAGMENT);
                // Handle on Back
                tagsToSearch.setText("");
            }
        });
        return view;
    }
}