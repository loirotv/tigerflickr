package com.tigerspike.interview.oriolgarcia.tigerflickr.controllers.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.tigerspike.interview.oriolgarcia.tigerflickr.R;
import com.tigerspike.interview.oriolgarcia.tigerflickr.controllers.adapters.PictureAdapter;
import com.tigerspike.interview.oriolgarcia.tigerflickr.models.PictureModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by OriolGarcia on 18/7/17.
 */
public class GalleryFragment extends BaseFragment {

    private RecyclerView picturesList;
    private final String TAG_REQUEST = "MY_TAG";
    private PictureAdapter pictureAdapter;
    private RequestQueue mVolleyQueue;
    private ArrayList<PictureModel> pictures = new ArrayList<>();
    private TextView noResults;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity.setFragment(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gallery_fragment, container, false);

        // Initialise Volley Request Queue.
        mVolleyQueue = Volley.newRequestQueue(mActivity);

        picturesList = (RecyclerView) view.findViewById(R.id.pictures_list);
        setAdapter();

        noResults = (TextView) view.findViewById(R.id.no_results);

        mActivity.showProgress();
        makeFlickrAPIHttpRequest();

        return view;
    }

    /**
     * Allows to set the adapter for the RecyclerView
     */
    public void setAdapter() {
        final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        picturesList.setLayoutManager(llm);
        pictureAdapter = new PictureAdapter(mActivity, pictures, getActivity());
        picturesList.setAdapter(pictureAdapter);
    }

    /**
     * Makes the Http Request to the Flickr API
     */
    public void makeFlickrAPIHttpRequest() {

        String url = "https://api.flickr.com/services/feeds/photos_public.gne";
        Uri.Builder builder = Uri.parse(url).buildUpon();
        builder.appendQueryParameter("api_key", "f0e7c6c6ccda1f90088ce0a845baf385");
        builder.appendQueryParameter("format", "json");
        builder.appendQueryParameter("nojsoncallback", "1");
        builder.appendQueryParameter("tags", Arrays.toString(mActivity.getTagsToSearch().toArray()).replace("[", "").replace("]", ""));

        JsonObjectRequest jsonObjRequest = new JsonObjectRequest(Request.Method.GET, builder.toString(), null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    parseFlickrImageResponse(response);
                    pictureAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                    mActivity.showToast("JSON parse error");
                }
                mActivity.stopProgress();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                mActivity.stopProgress();
                mActivity.showToast(getResources().getString(R.string.no_internet));
            }
        });

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions. Volley does retry for you if you have specified the policy.
        jsonObjRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjRequest.setTag(TAG_REQUEST);
        mVolleyQueue.add(jsonObjRequest);
    }


    /**
     * Parses the JSON response in order to get Flickr pictures
     *
     * @param response
     * @throws JSONException
     */
    private void parseFlickrImageResponse(JSONObject response) throws JSONException {

        if (response.has("items")) {
            try {
                JSONArray items = response.getJSONArray("items");

                for (int index = 0; index < items.length(); index++) {
                    JSONObject jsonObj = items.getJSONObject(index);

                    String title = jsonObj.getString("title");
                    String published = jsonObj.getString("published");
                    String author = jsonObj.getString("author");
                    String[] tagsArray = jsonObj.getString("tags").split(" ", -1);
                    ArrayList<String> tags = new ArrayList<>(Arrays.asList(tagsArray));

                    JSONObject jsonMedia = jsonObj.getJSONObject("media");
                    String photoMediaUrl = jsonMedia.getString("m");
                    String imageUrl = photoMediaUrl.replaceFirst("_m.", "_b.");

                    PictureModel pictureModel = new PictureModel(title, published, author, imageUrl, tags);
                    pictures.add(pictureModel);
                }
                if (pictures.size() > 0) {
                    picturesList.setVisibility(View.VISIBLE);
                    noResults.setVisibility(View.GONE);
                    setAdapter();
                } else {
                    picturesList.setVisibility(View.GONE);
                    noResults.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
