package com.tigerspike.interview.oriolgarcia.tigerflickr;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.tigerspike.interview.oriolgarcia.tigerflickr.controllers.fragments.GalleryFragment;
import com.tigerspike.interview.oriolgarcia.tigerflickr.controllers.fragments.InspirationFragment;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private static final int GALLERY_FRAGMENT = 0;
    private static final int INSPIRATION_FRAGMENT = 1;

    private Fragment mFragment;
    private FragmentManager fm = getFragmentManager();
    private ProgressDialog mProgress;
    private ArrayList<String> tagsToSearch;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchView = (SearchView) findViewById(R.id.search);
        // Sets searchable configuration defined in searchable.xml for this SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        searchEditText.setHintTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));

        // Manages search intent
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            setTagsToSearch(new ArrayList<>(Arrays.asList(query.split(" "))));
            setFragment(GALLERY_FRAGMENT);
        } else {
            setFragment(INSPIRATION_FRAGMENT);
        }

    }

    /**
     * Replaces main_frame_layout_container by the fragment (id) received by param
     *
     * @param whichFragment
     */
    public void setFragment(int whichFragment) {
        FragmentTransaction ft = fm.beginTransaction();
        Fragment frag;

        switch (whichFragment) {
            case INSPIRATION_FRAGMENT:
                searchView.setVisibility(View.GONE);
                frag = new InspirationFragment();
                ft.replace(R.id.main_frame_layout_container, frag);
                ft.commit();
                setFragment(frag);
                break;
            case GALLERY_FRAGMENT:
                searchView.setVisibility(View.VISIBLE);
                frag = new GalleryFragment();
                ft.replace(R.id.main_frame_layout_container, frag);
                ft.addToBackStack("GalleryFragment").commit();
                setFragment(frag);
                break;
        }
    }

    /**
     * Manages searchview visibility when onBackPressed
     */
    @Override
    public void onBackPressed() {
        if (getFragment() instanceof GalleryFragment) {
            searchView.setVisibility(View.GONE);
            setFragment(INSPIRATION_FRAGMENT);
        } else {
            finish();
        }
    }

    /**
     * Sets variable mFragment by the fragment received by param
     *
     * @param fragment
     */
    public void setFragment(android.app.Fragment fragment) {
        mFragment = fragment;
    }

    /**
     * Getter of mFragment variable
     *
     * @return
     */
    public Fragment getFragment() {
        return mFragment;
    }

    /**
     * Setter tags to search at the API
     *
     * @param tags
     */
    public void setTagsToSearch(ArrayList<String> tags) {
        this.tagsToSearch = tags;
    }

    /**
     * Getter tags to search at the API
     *
     * @return
     */
    public ArrayList<String> getTagsToSearch() {
        return tagsToSearch;
    }

    public void showProgress() {
        mProgress = ProgressDialog.show(this, "", getResources().getString(R.string.loading));
    }

    public void stopProgress() {
        mProgress.cancel();
    }

    public void showToast(String msg) {
        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStop() {
        super.onStop();
        if (mProgress != null)
            mProgress.dismiss();
    }

}
