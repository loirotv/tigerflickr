package com.tigerspike.interview.oriolgarcia.tigerflickr.controllers.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tigerspike.interview.oriolgarcia.tigerflickr.MainActivity;

/**
 * Created by OriolGarcia on 18/7/17.
 */
public class BaseFragment extends Fragment {

    protected View view;
    protected static MainActivity mActivity;
    protected Fragment mFragment, rFragment;

    public BaseFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
        mFragment = this;
        rFragment = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}

